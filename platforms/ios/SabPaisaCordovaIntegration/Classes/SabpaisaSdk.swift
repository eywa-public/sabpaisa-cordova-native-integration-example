//
//  SabpaisaSdk.swift
//  SabPaisaCordovaIntegration
//
//  Created by Lokesh D on 04/02/23.
//

import Foundation
import UIKit
import SabPaisa_IOS_Sdk

@objc class SabpaisaSdk: NSObject{
    
    var initUrl="https://sdkstaging.sabpaisa.in/SabPaisa/sabPaisaInit?v=1"
    var baseUrl="https://sdkstaging.sabpaisa.in"
    var transactionEnqUrl="https://stage-txnenquiry.sabpaisa.in"
    
    @objc override init(){
        
    }
    
    @objc func openSdk(_ firstName:String,lastname lastName:String,email emailid:String,mobilenumber mobileNumber:String,amountValue amount:String){
        print("hello")
        
        let bundle = Bundle(identifier: "com.eywa.ios.SabPaisa-IOS-Sdk")
        let storyboard = UIStoryboard(name: "Storyboard", bundle: bundle)
        
        
        let secKey="kaY9AIhuJZNvKGp2"
        let secInivialVector="YN2v8qQcU3rGfA1y"
        let transUserName="rajiv.moti_336"
        let transUserPassword="RIADA_SP336"
        let clientCode="TM001"
        
        
        let vc = storyboard.instantiateViewController(withIdentifier: "InitialLoadViewController_Identifier") as! InitialLoadViewController
        vc.sdkInitModel=SdkInitModel(firstName: firstName, lastName: lastName, secKey: secKey, secInivialVector: secInivialVector, transUserName: transUserName, transUserPassword: transUserPassword, clientCode: clientCode, amount: Float(amount)!,emailAddress: emailid,mobileNumber: mobileNumber,isProd: false,baseUrl: baseUrl, initiUrl: initUrl,transactionEnquiryUrl: transactionEnqUrl)
        
        
        vc.callback =  { (response:TransactionResponse)  in
            print("---------------Final Response To USER------------")

            vc.dismiss(animated: true)
        }
        
        if let topController = UIApplication.shared.keyWindow?.rootViewController {
            topController.present(vc, animated: true, completion: nil)
        }
        
        
    }
}
