/********* SabpaisaCordovaPlugin.m Cordova Plugin Implementation *******/

#import <Cordova/CDV.h>
#import <SabPaisaCordovaIntegration-Swift.h>

@interface SabpaisaCordovaPlugin : CDVPlugin {
  // Member variables go here.
}

- (void)coolMethod:(CDVInvokedUrlCommand*)command;
@end

@implementation SabpaisaCordovaPlugin

- (void)coolMethod:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult* pluginResult = nil;
    NSString* firstName = [command.arguments objectAtIndex:0];
    NSString* lastName = [command.arguments objectAtIndex:1];
    NSString* emailId = [command.arguments objectAtIndex:2];
    NSString* mobileNumber = [command.arguments objectAtIndex:3];
    NSString* amount = [command.arguments objectAtIndex:4];
    

    //
    
    // BEGIN added by NDH 20210430
    SabpaisaSdk* mySwiftClass = [[SabpaisaSdk alloc]init];

    [mySwiftClass openSdk:firstName lastname:lastName email:emailId mobilenumber:mobileNumber amountValue:amount ];
        
    
    //
    
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

@end
